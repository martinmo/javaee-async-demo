/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mm.javaee.async.asyncdemo.ejbs;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author martin
 */

@Stateless
public class RequestLoggerEJB {
    
    @PersistenceContext
    EntityManager em;
    
    
    Logger log = LoggerFactory.getLogger(RequestLoggerEJB.class);
    
    public void log(String message){
        
        log.info("EJB method call");
        
        RequestLogEntry requestLogEntry = new RequestLogEntry();
        
        requestLogEntry.setMessage(message);
        
        em.persist(requestLogEntry);
    }
}
