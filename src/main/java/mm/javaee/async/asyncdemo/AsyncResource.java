package mm.javaee.async.asyncdemo;

import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import java.util.function.BiConsumer;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedScheduledExecutorService;
import mm.javaee.async.asyncdemo.ejbs.RequestLoggerEJB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/async")
public class AsyncResource {

    @Inject
    RemoteService remoteService;

    @Resource
    private ManagedScheduledExecutorService mes;
    
    @Inject
    RequestLoggerEJB logger;
    
    private final static Logger log = LoggerFactory.getLogger(AsyncResource.class);

    @GET
    @Path("/plain")
    public void async(@Suspended AsyncResponse response, @QueryParam("exception") boolean throwException) {
        log.info("request incoming");
        remoteService.getServerTime(throwException).thenApply(res -> {
            log.info("got response from service");
            logger.log(res);
            return res;
        }).whenCompleteAsync(writeResponse(response));
    }

    @GET
    @Path("/sync")
    public String sync() {
        return remoteService.getServerTimeSync();
    }

    @GET
    @Path("/remote")
    public void asyncRemote(@Suspended AsyncResponse response) {
        remoteService.getRemoteServerTime().whenCompleteAsync(writeResponse(response));
    }

    @GET
    @Path("/remotesync")
    public String syncRemote() {
        return remoteService.getRemoteServerTimeSync();
    }

    @GET
    @Path("/delay")
    public void delay(@Suspended AsyncResponse response) {
        mes.schedule(() -> {
            System.out.println("running");
            response.resume("delay");
        }, 2, TimeUnit.SECONDS);
    }

    private static <T> BiConsumer<T, Throwable> writeResponse(AsyncResponse response) {
        log.info("writing response");
        return (time, exception) -> {
            if (time != null) {
                response.resume(time);
            } else {
                response.resume(exception);
            }
        };
    }

}
