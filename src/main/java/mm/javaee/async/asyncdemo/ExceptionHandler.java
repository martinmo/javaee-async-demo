/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mm.javaee.async.asyncdemo;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author martin
 */
@Provider
public class ExceptionHandler implements ExceptionMapper<Exception>{

        Logger log = LoggerFactory.getLogger(ExceptionHandler.class);

    @Override
    public Response toResponse(Exception exception) {
        System.out.println("handle");
        log.error("handle", exception);
        return Response.status(Status.INTERNAL_SERVER_ERROR).entity("handled exception: " + exception.getMessage()).build();
    }
    
}
