package mm.javaee.async.asyncdemo;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.InvocationCallback;
import javax.ws.rs.client.WebTarget;
import java.time.ZonedDateTime;
import java.util.concurrent.CompletableFuture;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemoteService {
    
    Logger log = LoggerFactory.getLogger(RemoteService.class);

    public CompletableFuture<String> getServerTime(boolean throwException){
        if(throwException){
            final CompletableFuture<String> future = new CompletableFuture<>();
            future.completeExceptionally(new RuntimeException("planed exception"));
            return future;
        }else{
            return CompletableFuture.completedFuture(ZonedDateTime.now().toString());
        }
    }
    
    public String getServerTimeSync(){
        return ZonedDateTime.now().toString();
    }
    
    public String getRemoteServerTimeSync(){

        final Client client = ClientBuilder.newClient();
        final WebTarget target = client.target("http://127.0.0.1:8180/javaee-async-demo/api/async/delay");

        return target.request().get(String.class);
    }

    public CompletableFuture<String> getRemoteServerTime(){

        final Client client = ClientBuilder.newClient();
        final WebTarget target = client.target("http://127.0.0.1:8180/javaee-async-demo/api/async/delay");

        final CompletableFuture<String> result = new CompletableFuture<>();

        target.request().async().get(new InvocationCallback<String>() {
            @Override
            public void completed(String o) {
                log.info("WsClient got OK response");
                result.complete(o);
            }

            @Override
            public void failed(Throwable throwable) {
                log.info("WsClient failed");
                result.completeExceptionally(throwable);
            }
        });

        return result;
    }

}
